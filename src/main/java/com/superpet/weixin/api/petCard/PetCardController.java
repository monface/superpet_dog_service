package com.superpet.weixin.api.petCard;

import com.jfinal.kit.Ret;
import com.superpet.common.controller.BaseController;
import com.superpet.common.kits.ConstantKit;
import com.superpet.common.kits.DateKit;
import com.superpet.common.model.PetCard;

import java.util.Date;
import java.util.List;

public class PetCardController extends BaseController {

    static PetCardService srv = PetCardService.me;

    public void savePetCard(){
        Long petCardId = getParaToLong("petCardId",0L);
        PetCard petCard = PetCard.dao.findById(petCardId);
        if(petCard==null){
            petCard = new PetCard();
            petCard.setUserid(getLoginUserId());
        }
        petCard.setCardPic(getPara("cardPic"));
        petCard.setCardContent(getPara("cardContent"));
        petCard.setMasterName(getPara("masterName"));
        petCard.setPetName(getPara("petName"));
        petCard.setUpdateTime(DateKit.toTimeStr(new Date()));
        boolean result;
        if(petCard.getId()==null){
            result = petCard.save();
        }else{
            result = petCard.update();
        }
        if(result){
            renderJson(Ret.ok("code", ConstantKit.CODE_SUCCESS).set("msg",ConstantKit.MSG_SUCCESS));
        }else{
            renderJson(Ret.ok("code", ConstantKit.CODE_FAIL).set("msg",ConstantKit.MSG_FAIL));
        }
    }

    public void getPetCardList(){
        List<PetCard> cards = srv.getPetCardList(getLoginUserId());
        renderJson(Ret.ok("code", ConstantKit.CODE_SUCCESS).set("msg",ConstantKit.MSG_SUCCESS).set("cards",cards));
    }

}
